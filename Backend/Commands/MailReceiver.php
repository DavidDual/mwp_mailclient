<?php

namespace App\Services\MWP_Mail\Backend\Commands;

use Illuminate\Console\Command;
use App\Services\MWP_Mail\Backend\Controllers\MailClient;

class MailReceiver extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'mailReceiver {type} {nr?} {--u|username=} {--p|password=} {--i|inbox=INBOX} {--port=993} {--ssl=true} {--d|domain=localhost} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the scheduled commands';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $imap = "{";
        $imap .= $this->option('domain');
        $imap .= ":";
        $imap .= $this->option('port');
        $imap .= "/imap";
        if($this->option('ssl')){
            $imap .= "/ssl";
        }
        $imap .= "}";

        $domain = $this->option('domain');
        $username = $this->option('username');
        $password = $this->option('password');

        switch ($this->argument('type')) {
            case "list":
                $mail = new MailClient($domain, $username, $password);
                dd($mail->getMailBoxes());
                break;
            case "inbox":
                $mail = new MailClient($domain, $username, $password);
                dd($mail->listMessage(7, $this->argument('nr')));
                break;
            case "msg":
                $mail = new MailClient($domain, $username, $password);
                dd($mail->getMessage($this->argument('nr')));
                break;
        }
    }
}
