<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 4/2/18
 * Time: 8:09 AM
 */

namespace App\Services\MWP_Mail\Backend\Controllers;

use App\Http\Controllers\Controller;

class MailClient extends Controller
{
    private $stream;
    private $ref;

    public function __construct($domain = "my-server.nl", $username = "info@d-consultancy.nl", $password = "Aa111111@", $port = 993, $ssl = true)
    {
        $this->ref = "{" . $domain . ":" . $port . "/imap";
        if ($ssl) {
            $this->ref .= "/ssl";
        }
        $this->ref .= "}";
        $this->stream = \imap_open($this->ref, $username, $password, OP_READONLY)
        or die("can't connect: " . imap_last_error());
    }

    public function open($mailbox)
    {
        \imap_reopen($this->stream, $this->ref . $mailbox, OP_READONLY)
        or die("can't connect: " . imap_last_error());
    }

    public function __destruct()
    {
        \imap_close($this->stream);
    }

    public function getMailBoxes()
    {
        $raw = \imap_getmailboxes($this->stream, $this->ref, "*");
        $boxes = [];
        foreach ($raw as $box) {
            $status = \imap_status($this->stream, $box->name, SA_ALL);
            $status->name = str_replace($this->ref, "", $box->name);
            array_push($boxes, $status);
        }

        return array_reverse($boxes);
    }

    public function listMessage($range, $page = 0)
    {
//        return imap_sort($this->stream, SORTARRIVAL, 1, SE_UID | SE_NOPREFETCH);

        $max = imap_num_msg($this->stream);
        $start = $max - $range * $page;
        $stop = $max - $range * $page - $range;
        if ($start < 1) {
            return false;
        }
        if ($stop < 1) {
            $stop = 1;
        }
        $msgs = \imap_fetch_overview($this->stream, $start . ":" . $stop);
        foreach ($msgs as $msg) {
            $msg->subject = imap_utf8($msg->subject);
            if (isset($msg->from)) {
                $msg->from = imap_utf8($msg->from);
                $res = [];
                preg_match("/^(.*) <(.*)>$/", $msg->from, $res);
                if (isset($res[1]) && isset($res[2])) {
                    $msg->from = ["name" => $res[1], "email" => $res[2]];
                } else {
                    $msg->from = ["name" => $msg->from, "email" => $msg->from];
                }
            }
            $msg->to = imap_utf8($msg->to);
        }
        return array_reverse($msgs);
    }

    public function getMessage($id)
    {
        $structure = \imap_fetchstructure($this->stream, $id, FT_UID);
        $parts = [];
        foreach ($structure->parts as $part) {
            if ($part->ifdisposition == 0) {
                array_push($parts, $part->subtype);
            }
        }
        return imap_qprint(\imap_fetchbody($this->stream, $id, count($parts), FT_PEEK | FT_UID));
    }

    public function index()
    {
        return response()->json(['service' => 'MWP_Mail', 'type' => 'Inbox', 'props' => ['title' => 'Mailbox',
            'mailboxes' => $this->getMailBoxes()]]);
    }

    public function messages($range, $page, $mailbox = "INBOX")
    {
        $this->open($mailbox);
        return response()->json($this->listMessage($range, $page));
    }
}
